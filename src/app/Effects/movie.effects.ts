import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import * as MovieActions from '../Actions/movie.action';
import Movies from '../Models/movie.model';

@Injectable()
export class MovieEffects {
  constructor(private http: HttpClient, private action$: Actions) {}

  private ApiURL: string = 'https://swapi.dev/api';

  GetMovies$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(MovieActions.BeginGetMovieAction),
      mergeMap(action =>
        this.http.get(`${this.ApiURL}/films/`).pipe(
          map((data: any) => {
            let movies: Movies[] = [];

            if (data.results && data.results.length > 0) {
              for (let i = 0; i < data.results.length; i++) {
                let { title, producer, director, release_date, opening_crawl, characters } = data.results[i];
                let people = characters.map(urlString => urlString.replace('http://swapi.dev/api/people/', ''));
                movies[i] = {
                  id: i+1,
                  title,
                  producer,
                  director,
                  released: release_date,
                  crawl: opening_crawl,
                  characterIds: people.map(peopleString => peopleString.replace('/', ''))
                }
              }
            }

            return MovieActions.SuccessGetMovieAction({ payload: movies });
          }),
          catchError((error: Error) => {
            return of(MovieActions.ErrorGetMovieAction(error));
          })
        )
      )
    )
  );
}
