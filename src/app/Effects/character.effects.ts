import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import * as CharacterActions from '../Actions/character.action';
import Characters from '../Models/character.model';

@Injectable()
export class CharacterEffects {
  constructor(private http: HttpClient, private action$: Actions) {}

  private ApiURL: string = 'https://swapi.dev/api';

  GetMovies$: Observable<Action> = createEffect(() =>
    this.action$.pipe(
      ofType(CharacterActions.BeginGetCharactersAction),
      mergeMap(action =>
        this.http.get(`${this.ApiURL}/people/`).pipe(
          map((data: any) => {
            let characters: Characters[] = [];

            if (data.results && data.results.length > 0) {
              for (let i = 0; i < data.results.length; i++) {
                let movies = data.results[i].films.map(urlString => urlString.replace('http://swapi.dev/api/films/', ''));
                characters[i] = {
                  id: i+1,
                  name: data.results[i].name,
                  height: data.results[i].height,
                  mass: data.results[i].mass,
                  born: data.results[i].birth_year,
                  gender: data.results[i].gender,
                  films: movies.map(filmsString => filmsString.replace('/', ''))
                };
              }
            }

            return CharacterActions.SuccessGetCharactersAction({ payload: characters });
          }),
          catchError((error: Error) => {
            return of(CharacterActions.ErrorGetCharactersAction(error));
          })
        )
      )
    )
  );
}
