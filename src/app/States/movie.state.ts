import Movie from '../Models/movie.model';

export default class MoviesState {
  Movies: Array<Movie>;
  MovieError: Error;
}

export const initializeState = (): MoviesState => {
  return {
    Movies: Array<Movie>(),
    MovieError: null
  };
};
