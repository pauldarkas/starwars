import Character from '../Models/character.model';

export default class CharacterState {
  Characters: Array<Character>;
  CharacterError: Error;
}

export const initializeState = (): CharacterState => {
  return {
    Characters: Array<Character>(),
    CharacterError: null,
  };
};
