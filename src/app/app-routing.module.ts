import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MoviesComponent } from './Components/movies/movies.component';
import { MovieDetailsComponent } from './Components/movie-details/movie-details.component';
import { CharacterComponent } from './Components/character/character.component';
import { NotFoundComponent } from './Components/not-found/not-found.component';

const routes: Routes = [
  {path: '', component: MoviesComponent},
  {path: 'movie/:id', component: MovieDetailsComponent},
  {path: 'character/:id', component: CharacterComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
