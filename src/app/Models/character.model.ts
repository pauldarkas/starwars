import Movie from './movie.model';

export default interface Character {
  id: number;
  name: string;
  height: string;
  mass: string;
  born: string;
  gender: string;
  films: Array<string>
}
