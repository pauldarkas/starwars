import Character from './character.model';

export default interface Movie {
  id: number;
  title: string;
  producer: string;
  director: string;
  released: string;
  crawl: string;
  characterIds: Array<string>
}
