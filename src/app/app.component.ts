import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import Movie from './Models/movie.model';
import * as MovieActions from './Actions/movie.action';
import Character from './Models/character.model';
import * as CharacterActions from './Actions/character.action';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  movies: Observable<Movie[]>;
  moviesCount: number;
  characters: Observable<Character[]>;
  charactersCount: number;

  constructor(
    private _store: Store<any>
  ) {
    _store.subscribe( state => {
      this.movies = state.movies.Movies;
      this.moviesCount = state.movies.Movies.length;
      this.characters  = state.characters.Characters;
      this.charactersCount = state.characters.Characters.length;
    });

    if (this.moviesCount <= 0) {
      this._store.dispatch(MovieActions.BeginGetMovieAction());
    }

    if (this.charactersCount <= 0) {
      this._store.dispatch(CharacterActions.BeginGetCharactersAction());
    }
  }



  title = 'starwars';
}
