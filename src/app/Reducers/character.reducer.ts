import { Action, createReducer, on } from '@ngrx/store';
import * as CharacterActions from '../Actions/character.action';
import CharacterState, { initializeState } from '../States/character.state';

export const initialState = initializeState();

const reducer = createReducer(
  initialState,
  on(CharacterActions.GetCharactersAction, (state: CharacterState) => {
    return { ...state }
  }),
  on(CharacterActions.SuccessGetCharactersAction, (state: CharacterState, { payload }) => {
    return { ...state, Characters: payload };
  }),
  on(CharacterActions.ErrorGetCharactersAction, (state: CharacterState, error: Error) => {
    return { ...state, CharacterError: error };
  })
);

export function CharactersReducer(state: CharacterState | undefined, action: Action) {
  return reducer(state, action);
}
