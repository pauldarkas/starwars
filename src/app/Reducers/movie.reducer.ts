import { Action, createReducer, on } from '@ngrx/store';
import * as MovieActions from '../Actions/movie.action';
import MoviesState, { initializeState } from '../States/movie.state';

export const initialState = initializeState();

const reducer = createReducer(
  initialState,
  on(MovieActions.GetMoviesAction, (state: MoviesState) => {
    return { ...state }
  }),
  on(MovieActions.SuccessGetMovieAction, (state: MoviesState, { payload }) => {
    return { ...state, Movies: payload };
  }),
  on(MovieActions.ErrorGetMovieAction, (state: MoviesState, error: Error) => {
    return { ...state, MovieError: error };
  })
);

export function MovieReducer(state: MoviesState | undefined, action: Action) {
  return reducer(state, action);
}
