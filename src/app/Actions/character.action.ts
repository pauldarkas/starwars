import { createAction, props } from '@ngrx/store';
import Character from '../Models/character.model';

export const GetCharactersAction = createAction('[Character] - Get Character');

export const BeginGetCharactersAction = createAction('[Character] - Begin Get Character');

export const SuccessGetCharactersAction = createAction(
  '[Character] - Success Get Character',
  props<{ payload: Character[] }>()
);

export const ErrorGetCharactersAction = createAction('[Character] - Character', props<Error>());
