import { createAction, props } from '@ngrx/store';
import Movie from '../Models/movie.model';

export const GetMoviesAction = createAction('[Movie] - Get Movie');

export const BeginGetMovieAction = createAction('[Movie] - Begin Get Movie');

export const SuccessGetMovieAction = createAction(
  '[Movie] - Success Get Movie',
  props<{ payload: Movie[] }>()
);

export const ErrorGetMovieAction = createAction('[Movie] - Error', props<Error>());
