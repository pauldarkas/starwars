import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import Movie from '../../Models/movie.model';
import MovieState from '../../States/movie.state';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  constructor(private _store: Store<any>) {
    this.movie$ = _store.pipe(select('movies'));
  }

  ngOnInit() {
    this.MovieSubscription = this.movie$
      .pipe(
        map(x => {
          this.MovieList = x.Movies;
          this.MovieError = x.MovieError;
          this.numOfMovies = x.Movies.length;
        })
      )
      .subscribe();
  }

  movie$: Observable<MovieState>;
  MovieSubscription: Subscription;
  MovieList: Movie[] = [];
  numOfMovies: number;

  MovieError: Error = null;
}
