import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import MovieState from '../../States/movie.state';
import Movie from '../../Models/movie.model';
import CharactersState from '../../States/character.state';
import Character from '../../Models/character.model';

@Component({
  selector: 'app-character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {

  constructor(private route: ActivatedRoute, private _store: Store<any>) { }

  ngOnInit(): void {
    this.selectedId = this.route.snapshot.paramMap.get('id');
    this._store.select('characters').pipe(
      map(x => {
        this.selectedCharacter = x.Characters.find(x => x.id === parseInt(this.selectedId));
        this.CharacterError = x.CharacterError;
      })
    ).subscribe();

    this._store.select('movies').pipe(
      map(x => {
        this.movies = x.Movies.filter(x => x.characterIds.find(character => character === this.selectedId));
        this.MovieError = x.MovieError;
      })
    ).subscribe();
  }

  selectedCharacter: Character;
  movies: Array<Movie>
  selectedId: string;
  MovieError: Error = null;
  CharacterError: Error = null;

}
