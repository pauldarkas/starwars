import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import MovieState from '../../States/movie.state';
import Movie from '../../Models/movie.model';
import CharactersState from '../../States/character.state';
import Character from '../../Models/character.model';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {
  constructor(private route: ActivatedRoute, private _store: Store<any>) { }

  ngOnInit(): void {
    this.selectedId = this.route.snapshot.paramMap.get('id');
    this._store.select('movies').pipe(
      map(x => {
        this.selectedMovie = x.Movies.find(x => x.id === parseInt(this.selectedId));
        this.MovieError = x.MovieError;
      })
    ).subscribe();

    this._store.select('characters').pipe(
      map(x => {
        this.characters = x.Characters.filter(x => x.films.find(movie => movie === this.selectedId));
        this.CharacterError = x.CharacterError;
      })
    ).subscribe();
  }

  selectedMovie: Movie;
  characters: Array<Character>
  selectedId: string;
  MovieSubscription: Subscription;
  MovieError: Error = null;
  CharacterError: Error = null;
}
