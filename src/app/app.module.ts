import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MoviesComponent } from './Components/movies/movies.component';
import { MovieDetailsComponent } from './Components/movie-details/movie-details.component';
import { CharacterComponent } from './Components/character/character.component';
import { NotFoundComponent } from './Components/not-found/not-found.component';
import { MovieReducer } from './Reducers/movie.reducer';
import { MovieEffects } from './Effects/movie.effects';
import { CharactersReducer } from './Reducers/character.reducer';
import { CharacterEffects } from './Effects/character.effects';

@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    MovieDetailsComponent,
    CharacterComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({ movies: MovieReducer, characters: CharactersReducer }),
    EffectsModule.forRoot([MovieEffects, CharacterEffects])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
